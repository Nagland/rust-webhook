extern crate rustc_serialize;

use std::env;
use std::fs::File;
use std::path::Path;
use std::string::String;
use std::error::Error;
use std::io::prelude::*;
use rustc_serialize::{Decodable, Encodable, json};

#[derive(RustcDecodable, RustcEncodable)]
pub struct WatchItem {
    repo:   String,
    branch: String,
    script: String
}

#[derive(RustcDecodable, RustcEncodable)]
pub struct Config  {
    bind: String,
    items: Vec<WatchItem>
}


fn main() {

    let mut config = load_config("examples/hook.json");

    for s in config.items {
        println!("{:?}", s.repo);
    }
}

pub fn load_config(filename : &str) -> Config  {
    let path = Path::new(filename);
    let display = path.display();

    let mut file = match File::open(&path) {
        Ok(file) => file,
        Err(why) => panic!("Could not open {}: {}", display, Error::description(&why)),
    };

    let mut contents = String::new();

    match file.read_to_string(&mut contents) {
        Ok(len) => {},
        Err(_) => panic!("convert error"),
    };  

    println!("{}", contents); 

    let ret: Config= json::decode(&contents).unwrap();

    ret
}



